# rentlive-backend

#### 介绍
用SpringCloud搭建的后台，具有登录，展示，全局检索，评论，订单等登录功能，异步缓存。
采用Erueka，SpringCloudGateway，mongoDB，redis，ElstaticSearch，Nginx，RabbitMQ等组件。
可以在这基础上添加，定制化功能

#### 软件架构
软件架构说明


#### 安装教程

1.  用idea打开工程文件最外层的pom.xml即可

#### 使用说明

1.  需要提前准备MongoDB，Redis，RabbitMQ，docker的服务端
2.  关于以上服务端的配置，暂时还未上传

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
