package com.rentlive.buytime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuytimeApp {
    public static void main(String[] args) {
        SpringApplication.run(BuytimeApp.class, args);
    }
}
