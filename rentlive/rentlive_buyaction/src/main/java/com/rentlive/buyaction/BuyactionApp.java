package com.rentlive.buyaction;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuyactionApp {
    public static void main(String[] args) {
        SpringApplication.run(BuyactionApp.class, args);
    }
}
