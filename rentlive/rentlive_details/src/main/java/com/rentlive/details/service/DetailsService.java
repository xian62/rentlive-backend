package com.rentlive.details.service;

import com.rentlive.rentlive.pojo.Item;

// 商品详情服务接口
public interface DetailsService {
    // 主键查询商品
    Item getDetails(String id);
}
