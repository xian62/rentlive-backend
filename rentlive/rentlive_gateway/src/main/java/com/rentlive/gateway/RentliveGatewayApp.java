package com.rentlive.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RentliveGatewayApp {
    public static void main(String[] args) {
        SpringApplication.run(RentliveGatewayApp.class, args);
    }
}
