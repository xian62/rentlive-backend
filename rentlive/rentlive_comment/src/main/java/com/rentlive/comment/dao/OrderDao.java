package com.rentlive.comment.dao;

import com.rentlive.rentlive.pojo.Order;

// 订单数据访问接口
public interface OrderDao {
    Order findById(String orderId);

    void updateCommentState(String orderId, int commentState);
}
