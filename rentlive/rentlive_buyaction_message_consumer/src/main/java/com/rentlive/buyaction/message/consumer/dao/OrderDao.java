package com.rentlive.buyaction.message.consumer.dao;

import com.rentlive.rentlive.pojo.Order;

// 订单数据访问接口
public interface OrderDao {
    void save(Order order);
}
